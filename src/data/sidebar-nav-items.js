export default function() {
  return [
    {
      title: "Dashboard",
      to: "/dashboard",
      htmlBefore: '<i class="material-icons">dashboard</i>',
      htmlAfter: ""
    },
    {
      title: "Event",
      htmlBefore: '<i class="material-icons">event</i>',
      to: "/event",
    },
    {
      title: "Surveillance",
      htmlBefore: '<i class="material-icons">camera_roll</i>',
      to: "/video",
    },
    {
      title: "AI",
      htmlBefore: '<i class="material-icons">psychology</i>',
      to: "/ai",
    },
    {
      title: "Clips",
      htmlBefore: '<i class="material-icons">collections</i>',
      to: "/components-overview",
    },
    {
      title: "Table",
      htmlBefore: '<i class="material-icons">table_view</i>',
      to: "/tables",
    },
    {
      title: "Predictive",
      htmlBefore: '<i class="material-icons">analytics</i>',
      to: "/predictive",
    },
    {
      title: "Time Monitor",
      htmlBefore: '<i class="material-icons">history_toggle_off</i>',
      to: "/timemonitoring",
    },
    {
      title: "Errors",
      htmlBefore: '<i class="material-icons">error</i>',
      to: "/errors",
    },
    
  ];
}
