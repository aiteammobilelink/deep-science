import { configureStore } from "@reduxjs/toolkit";
import userReducer from "../Features/userSlice.js"

export default configureStore({
    reducer:{
        user: userReducer,
    }
})