import { useState,useEffect } from "react";
import React from "react";
import { css } from "@emotion/react";
import HashLoader
from "react-spinners/HashLoader";
import { BrowserRouter as Router, Route } from "react-router-dom";
import routes from "./routes";
import withTracker from "./withTracker";
import "bootstrap/dist/css/bootstrap.min.css";
// import './shards-dashboard/styles/shards-dashboards.1.1.0.min.css'
import "./shards-dashboard/styles/shards-dashboards.1.1.0.min.css";
import NewLog from "./components/Auth/NewLog.js"
import {useSelector} from "react-redux"
import {selectUser} from "./Features/userSlice.js"



const App = () => {
  const [error, setError] = useState(null);
  const [isLoaded, setIsLoaded] = useState(false);
  const [items, setItems] = useState([]);
  const [items1,setItems1] = useState([]);
  const [items2,setItems2] = useState([]);
  const user = useSelector(selectUser);  
  const override = css`
  display: block;
  margin: 0 auto;
  border-color: black;
`;
// useEffect(() => {
//   fetch("https://mis.mobilelinkusa.com/deepscienceapi/api/StoreTraffic/getDetail/")
//     .then(res => res.json())
//     .then(
//       (result) => {
//         setIsLoaded(true);
//         setItems(result.Success.Data);
//         },
//       (error) => {
//         setIsLoaded(true);
//         setError(error);
//       }
//     )
//     fetch('http://10.0.0.52:8081/api/predictive/',
//     {
//       method:'GET',
//       headers:{
//         'Content-Type': 'application/json',
//       }
//     })
//     .then(resp => resp.json())
//             .then(
//               (result) => {
//                 setIsLoaded(true);
//                 setItems1(result);
                
//         },
//         (error) => {
//           setIsLoaded(true);
//           setError(error);
//         }
//             )
//             fetch('http://172.18.18.3:8080/api/storestatus/',
//     {
//       method:'GET',
//       headers:{
//         'Content-Type': 'application/json',
//       }
//     })
//     .then(resp => resp.json())
//             .then(
//               (result) => {
//                 setIsLoaded(true);
//                 setItems2(result);
                
//         },
//         (error) => {
//           setIsLoaded(true);
//           setError(error);
//         }
//             )
     
// }, [])
// if (error){
//   return <div>Api is Not Working</div>
// }
// else if (!isLoaded){
  
//     return <> 
        
//       <div>
//       <HashLoader
 
//       color="EC0909"
//       loading={!isLoaded} 
     
//       size={150}/>
//       </div>
//       </>
  

// } else {
  return(
  <>
  {user ? ( 

    <Router basename={process.env.REACT_APP_BASENAME || ""}>
      <div>
        {routes.map((route, index) => {
          return (
            <Route
              key={index}
              path={route.path}
              exact={route.exact}
              component={withTracker(props => {
                return (
                  <route.layout >
                    <route.component items={items} fulldata={items1} timemonoter={items2} />
                  </route.layout>
                );
              })}
            />
          );
        })}
      </div>
    </Router>
   ):(<NewLog/>)
  }
  </>
  )
}
export default App;

// (<Loginform login={login} error={error}/>)