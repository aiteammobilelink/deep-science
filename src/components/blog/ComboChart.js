import React from "react";
import PropTypes from "prop-types";
import { Row, Col, Card, CardHeader, CardBody, Button } from "shards-react";

import RangeDatePicker from "../common/RangeDatePicker";
import Chart from "../../utils/chart";

class Combo extends React.Component {
  constructor(props) {
    super(props);

    this.canvasRef = React.createRef();
  }

  componentDidMount() {
    const chartOptions = {
      ...{
        responsive: true,
        legend: {
          position: "top"
        },
        elements: {
          line: {
            // A higher value makes the line look skewed at this ratio.
            tension: 0.3
          },
          point: {
            radius: 0
          }
        },
        scales: {
          xAxes: [
            {
              gridLines: false,
              ticks: {
                // callback(tick, index) {
                  // Jump every 7 values on the X axis labels to avoid clutter.
                //   return index % 7 !== 0 ? "" : tick;
                // }
              }
            }
          ],
          yAxes: [
            {
              ticks: {
                suggestedMax: 45,
                callback(tick) {
                  if (tick === 0) {
                    return tick;
                  }
                  // Format the amounts using Ks for thousands.
                  return tick > 999 ? `${(tick / 50).toFixed(1)}` : tick;
                }
              }
            }
          ]
        },
        hover: {
          mode: "nearest",
          intersect: false
        },
        tooltips: {
          custom: false,
          mode: "nearest",
          intersect: false
        }
      },
      ...this.props.chartOptions
    };

    const BlogUsersOverview = new Chart(this.canvasRef.current, {
      type: "LineWithLine",
      data: this.props.chartData,
      options: chartOptions
    });

    // They can still be triggered on hover.
    const buoMeta = BlogUsersOverview.getDatasetMeta(0);
    buoMeta.data[0]._model.radius = 0;
    buoMeta.data[
      this.props.chartData.datasets[0].data.length - 1
    ]._model.radius = 0;

    // Render the chart.
    BlogUsersOverview.render();
  }

  render() {
    const { title } = this.props;
    return (
    //   <Card small className="h-100 w-100">
        // <CardHeader className="border-bottom">
        //   <h6 className="m-0">{title}</h6>
        // </CardHeader>
        // <CardBody className="pt-0">
          <canvas
            height="120"
            ref={this.canvasRef}
            style={{ maxWidth: "100% !important"}}
          />
        // </CardBody>
    //   </Card>
    );
  }
}

Combo.propTypes = {
  /**
   * The component's title.
   */
  title: PropTypes.string,
  /**
   * The chart dataset.
   */
  chartData: PropTypes.object,
  /**
   * The Chart.js options.
   */
  chartOptions: PropTypes.object
};

Combo.defaultProps = {
  title: "Motion",
  chartData: {
    labels: ["9am","10am","11am","12am","1pm","2pm","3pm","4pm","5pm","6pm","7pm","8pm","9pm"],
    datasets: [
      {
        label: "Motion",
        fill: "start",
        data: [
          50,
          30,
          20,
          10,
          40,
          20,
          30,
          60,
          50,
          20,
          50,
          40,
          20,
          20,
          60,
          50,
          20,
          80,
          25,
          45,
          30,
          35,
          40,
          60,
          20,
          10,
          15,
          20,
          10,
          50
        ],
        backgroundColor: "rgba(0,123,255,0.1)",
        borderColor: "rgba(0,123,255,1)",
        pointBackgroundColor: "#ffffff",
        pointHoverBackgroundColor: "rgb(0,123,255)",
        borderWidth: 1.5,
        pointRadius: 0,
        pointHoverRadius: 3
      },
    //   {
    //     label: "Last Week",
    //     fill: "start",
    //     data: [
    //       380,
    //       430,
    //       120,
    //       230,
    //       410,
    //       740,
    //       472,
    //       219,
    //       391,
    //       229,
    //       400,
    //       203,
    //       301,
    //       380,
    //       291,
    //       620,
    //       700,
    //       300,
    //       630,
    //       402,
    //       320,
    //       380,
    //       289,
    //       410,
    //       300,
    //       530,
    //       630,
    //       720,
    //       780,
    //       1200
    //     ],
    //     backgroundColor: "rgba(255,65,105,0.1)",
    //     borderColor: "rgba(255,65,105,1)",
    //     pointBackgroundColor: "#ffffff",
    //     pointHoverBackgroundColor: "rgba(255,65,105,1)",
    //     borderDash: [3, 3],
    //     borderWidth: 1,
    //     pointRadius: 0,
    //     pointHoverRadius: 2,
    //     pointBorderColor: "rgba(255,65,105,1)"
    //   }
    ]
  }
};

export default Combo;
