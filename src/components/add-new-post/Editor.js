import React from "react";
import ReactQuill from "react-quill";
import { Card, CardBody, Form, FormInput, Container, Col, Row, Button } from "shards-react";
import './video.css'
import "react-quill/dist/quill.snow.css";
import "../../assets/quill.css";
import '../../assets/quill.css'

const Editor = () => (
  <>
  <Container fluid className="change">
  <Form className='search'>
  <FormInput size="lg" className="mb-3 str" placeholder="#Search your Address Item" 
      />  
  </Form>
    <Row>
      <Col>
      <div className="d-flex justify-content-around ">
      <h3 style={{color:"grey"}}>Group</h3>
      <Button className="btnColor">
      <span class="material-icons playwithit">add_circle</span>
       <span className="mb"> New Group </span>
      </Button>
      </div>
      </Col>
    </Row>
    <Row>
      <Col>
      <Button className="btnColor">
      <span class="material-icons">folder</span>
       <span className="mb"> Recently View (4)</span>
      </Button> <br />
      <Button className="btnColor mt-2">
      <span class="material-icons">create_new_folder</span>
       <span className="mb"> Favourite (0) </span>
      </Button>
      </Col> 
      <Col>
      
      </Col>
    </Row>
  </Container>
  </>
);

export default Editor;
