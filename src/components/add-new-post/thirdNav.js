import React,{ useStat } from "react";
import ReactQuill from "react-quill";
import { Card, CardBody, Form, FormInput, Container, Col, Row, Button } from "shards-react";
import './video.css'
import "react-quill/dist/quill.snow.css";
import "../../assets/quill.css";
import '../../assets/quill.css'

export default function ThirdNav() {
    return (
        <div className="top mt-2">
            <Container fluid className="mt-2">
            <Row>
                <Col className="col-lg-8 col-md-6 recent">
                    <h2>AI Based View</h2>
                </Col>
                {/* viewContent */}
                <Col className="col-lg-4 col-md-6 d-flex justify-content-end">
                    <div className="clr">
                    <h4>Views</h4>
                    <Button className="cl">
                    <span class="material-icons">view_list</span> 
                    <span className="pb"> List </span>
                    </Button>
                    </div>
                </Col>
            </Row>
            </Container>
        </div>
    )
}
