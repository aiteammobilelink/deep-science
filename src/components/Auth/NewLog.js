import React,{useState} from "react";
import { useDispatch } from "react-redux";
import {login} from "../../Features/userSlice.js"
import {
Button,
TextField,
Grid,
Paper,
AppBar,
Typography,
Toolbar,
Link,
} from "@material-ui/core";
import "./Login.css"
// import {BRAND_NAME} from '../constants'

const NewLog = () => {
    const [name,setName] = useState("")
    const [email,setEmail] = useState("")
    const [password,setPassword] = useState("")
    const dispatch = useDispatch()
    
    // const [details,setDetails] = useState({name:"", email:"" , password:""})
    const handleSubmit = (e) =>{
        e.preventDefault();

        dispatch(
            login({
            name:name,
            email:email,
            password:password,
            loggedIn:true
            
        })
        );
    }
        return (

        <div>

            <AppBar position="static" alignitems="center" color="">
                <Toolbar>
                    <Grid container justify="center" wrap="wrap">
                        <Grid item>
                            <Typography variant="h2">Deep Science AI</Typography>
                        </Grid>
                    </Grid>
                </Toolbar>
            </AppBar>
                <Grid container spacing={0} justify="center" direction="row">
                    <Grid item>
                        <Grid container direction="column" justify="center" spacing={2} className="login-form">
                            <Paper variant="elevation" elevation={2} className="login-background">
                    <Grid item className="d-flex justify-content-center">
                        <Typography component="h1" variant="h5" style={{marginBottom:"10px"}}>
                            SIGN IN
                        </Typography>
                    </Grid>
                    
                    <Grid item>
                        <form onSubmit={(e)=> handleSubmit(e)} >
                <Grid container direction="column" spacing={2}>
                <Grid item>
                    <TextField type="name" placeholder="Name"
                        fullWidth
                        variant="outlined"
                        onChange={(e) => setName(e.target.value)}
                        value={name}
                        required
                        autoFocus
                    />
                </Grid>
                <Grid item>
                    <TextField type="email" placeholder="Email"
                        fullWidth
                        variant="outlined"
                        onChange={(e)=> setEmail(e.target.value)}
                        value={email}
                        required
                        
                    />
                </Grid>
                <Grid item>
                    <TextField
                        type="password"
                        placeholder="Password"
                        fullWidth
                        variant="outlined"
                        onChange={(e)=> setPassword(e.target.value)} 
                        value={password}
                        required
                    />
                </Grid>
                <Grid item>
                    <Button
                        variant="contained"
                        color="primary"
                        type="submit"
                        className="button-block"
                    >
                    Submit
                    </Button>
                </Grid>
                </Grid>
                        </form>
                </Grid>
                <Grid item>

                </Grid>
                </Paper>
                </Grid>
                </Grid>
                </Grid>
        </div>
        );
}

export default NewLog;