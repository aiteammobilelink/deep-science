import React from "react";
import { Nav } from "shards-react";
import Notifications from "./Notifications";
import UserActions from "./UserActions";
import Dropdown from 'react-bootstrap/Dropdown'
import { useDispatch } from "react-redux";
import {logout} from "../../../../Features/userSlice.js"
const NavbarNav =() => {
  const dispatch = useDispatch()
  const handlelogout = (e) =>{
  e.preventDefault()
  dispatch(logout())
  }
  return(
  <>
  <Nav navbar className="border-left flex-row">
    <Notifications />
    <Dropdown >
    <Dropdown.Toggle caret  style={{backgroundColor:"#3F3F3F", border:"none"}}>
    <span className="d-none d-md-inline-block"  style={{color:'#fff'}}>ai@Mobilelinkusa.com</span>
    </Dropdown.Toggle>
  <Dropdown.Menu>
    <Dropdown.Item >Profile</Dropdown.Item>
    <Dropdown.Item className="text-danger">
            <a style={{borderColor:"none",}} onClick={(e)=> handlelogout(e)} > Logout</a>
    </Dropdown.Item>
  </Dropdown.Menu>
</Dropdown>
    {/* <UserActions /> */}
  </Nav>
  </>
)
  };
export default NavbarNav;