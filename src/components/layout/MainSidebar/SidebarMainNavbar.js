import React from "react";
import PropTypes from "prop-types";
import { Navbar, NavbarBrand } from "shards-react";
import './side.css'
import { Dispatcher, Constants } from "../../../flux";
import logo from '../../../images/logo.png'

class SidebarMainNavbar extends React.Component {
  constructor(props) {
    super(props);

    this.handleToggleSidebar = this.handleToggleSidebar.bind(this);
  }

  handleToggleSidebar() {
    Dispatcher.dispatch({
      actionType: Constants.TOGGLE_SIDEBAR
    });
  }

  render() {
    const { hideLogoText } = this.props;
    // const logo = "../"
    return (
      <div className="main-navbar">
        <Navbar
          className="align-items-stretch flex-md-nowrap p-0"
          type="light"
          style={{background:"#3f3f3f"}}
        >
          <NavbarBrand
            className="ml-2"
            href="#"
            style={{ lineHeight: "50px" }}
          >
            <div className="d-table m-auto" >
              <img
                id="main-logo"
                className="d-inline-block align-top mr-0"
                style={{ maxWidth: "25px", marginLeft:"30px"}}
                src={logo}
                alt="Shards Dashboard"
              />
              {/* {!hideLogoText && (
                <span className="d-none d-md-inline ml-1" style={{color:'#fff'}} >
                   Surveillance
                </span>
              )} */}
            </div>
          </NavbarBrand>
          {/* eslint-disable-next-line */}
          <a
            className="toggle-sidebar d-sm-inline d-md-none d-lg-none"
            onClick={this.handleToggleSidebar}
          >
            {/* <i className="material-icons">&#xE5C4;</i> */}
          </a>
        </Navbar>
      </div>
    );
  }
}

SidebarMainNavbar.propTypes = {
  /**
   * Whether to hide the logo text, or not.
   */
  hideLogoText: PropTypes.bool
};

SidebarMainNavbar.defaultProps = {
  hideLogoText: false
};

export default SidebarMainNavbar;
