import React,{useState}  from 'react'
// import VideoPlayer from "react-video-js-player"
import './cardResponsive.css';
// import VideoLooper from 'react-video-looper'
// import videojs from 'videojs'
const Videojs = ({video}) => {
    const videosrc=video;
    return (
        <div className="App">
            
            <VideoPlayer
            control = {true}
            src={videosrc}
            className="videoConfig"
            autoplay= {false}
            bigPlayButton={true}
            />
        </div>
    )
}

export default Videojs
