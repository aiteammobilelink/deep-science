import React from "react";
import {
  InputGroup,
  FormInput,
  Dropdown,
  DropdownToggle,
  DropdownMenu,
  DropdownItem
} from "shards-react";

class TDD extends React.Component {
  constructor(props) {
    super(props);

    this.toggle = this.toggle.bind(this);

    this.state = {
      dropdown1: false,
      dropdown2: false
    };
  }

  toggle(which) {
    const newState = { ...this.state };
    newState[which] = !this.state[which];
    this.setState(newState);
  }

  render() {
    return (
      <div>
        
          <select style={{background:'white', padding:"0px 50px 0px 50px", color:"black", border:'1px solid grey', textAlign:"center"}}>
            <option default>WaterFall</option>
            <option>Peek</option>
          </select>
      </div>
    );
  }
}

export default TDD;
