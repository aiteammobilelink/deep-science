import React from "react";
import { Row } from "react-bootstrap";
import { Col, FormRadio } from "shards-react";

const RadioButtons = () => (
  <Row>
  <Col sm="4" xl="12" md="12" lg="12" className="mb-3">
    <fieldset>
      <FormRadio>All</FormRadio><br />
      <FormRadio defaultChecked>Transactions</FormRadio><br />
      <FormRadio enabled>Motion</FormRadio><br />
      <FormRadio enabled className="mb-3"> 
        Bookmarks
      </FormRadio>
    </fieldset>
  </Col>
  </Row>
// another Row 

// another Row 
);

export default RadioButtons;
