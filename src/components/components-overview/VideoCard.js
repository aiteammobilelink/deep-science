import React from 'react'
import {Row,Col} from "shards-react";
import TDD from '../components-overview/TDD'
export default function VideoCard() {
    return (
        <div>
            <Row style={{background:"#CCCACA", borderTop:'3px solid grey',borderBottom:"3px solid grey"}}>
                <Col className="col-lg-6 col-md-6 col-sm-5 col-xl-6">
                <div style={{color:"black", margin:"5px 0px 5px 0px"}}>
                    Monday, 11 Oct, 2021-Sunday, 26 Sep, 2021
                </div>
    
                </Col>
                <Col className="col-lg-6 col-md-6 col-sm-6 col-xl-6">
                <div style={{color:"black",  margin:"5px 0px 5px 0px", display:"flex",justifyContent:"flex-end", width:"100%"}}>Card Layout &nbsp;
                <TDD /> 
                </div>
                </Col>
                </Row> 
        </div>
    )
}
