import React, { Component } from "react";
import Hls from "hls.js";
class Player extends Component {
    state = {};
        
      componentDidMount(props) {
        if (Hls.isSupported() && this.player) {
            const video = this.player;
            const hls = new Hls();
            hls.loadSource(
            "http://172.18.18.5:8080/hls/stream.m3u8");
            hls.attachMedia(video);
            hls.on(Hls.Events.MANIFEST_PARSED, function() {
            video.play();
            });
            }
            }
      _onTouchInsidePlayer() {
        if (this.player.paused) {
          this.player.play();
        } else {
          this.player.pause();
        }
      }
    
      render() {
        
         
          return (
            <video
            preload="none"
            className="videoCanvas"
            controls
            // onClick={this._onTouchInsidePlayer}
            ref={player => (this.player = player)}
            autoPlay={true}
        />
          );
        }
      }
export default Player;

