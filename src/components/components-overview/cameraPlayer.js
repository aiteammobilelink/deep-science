import React,{useRef} from 'react'
import {Row,Col  } from "shards-react";

import Button from 'react-bootstrap/Button'
import "../../views/Button.css"
import Card from 'react-bootstrap/Card'
import '../../views/scrollbar.css';
import Player from "./HLS_js/Hls_Js.js"
import Player1 from "./HLS_js/Hls_Js1.js"
import Player2 from "./HLS_js/Hls_Js2.js"



export default function CameraPlayer() {
    
    return (
    <>
        <div className="scrollbar scrollbar-juicy-peach">
            <Row className="mt-2">
            <Col className="col-lg-4 col-md-6 col-sm-12 col-xs-12 ">
                <Card>
                    {/* <Player/> */}
                    <Card.Body className="cardBody" >
                    <Card.Title>SunsetPlaza</Card.Title>
                    <Card.Text>
                        <div className="mt-2">
                        <Button className="btn-outline-success btp" style={{marginRight:"10px"}}>
                            Add Notes
                        </Button>
                        <Button className="btn-outline-success btp">
                            Save Clips
                        </Button>
                        </div>

                    </Card.Text>
                    </Card.Body>
                    <Card.Footer className="cf">
                    <small className="text-muted">Motion @Airline Shopping Center</small>
                    </Card.Footer>
                </Card>
            </Col>
            {/* 2nd Player */}
            <Col className="col-lg-4 col-md-6 col-sm-12 col-xs-12">
                <Card>
                    {/* <Player1/> */}
                    <Card.Body className="cardBody">
                    <Card.Title>WeslacoTX</Card.Title>
                    <Card.Text>
                        
                        <div className="mt-2">
                        <Button className="btn-outline-success btp" style={{marginRight:"10px"}}>
                            Add Notes
                        </Button>
                        <Button className="btn-outline-success btp">
                            Save Clips
                        </Button>
                        </div>

                    </Card.Text>
                    </Card.Body>
                    <Card.Footer className="cf">
                    <small className="text-muted">Motion @Airline Shopping Center</small>
                    </Card.Footer>
                </Card>
            </Col>
            {/* 3rd Player */}
            <Col className="col-lg-4 col-md-6 col-sm-12 col-xs-12">
                <Card>
                        {/* <Player2/> */}
                    <Card.Body className="cardBody">
                    <Card.Title>Perkins Rd</Card.Title>
                    <Card.Text>
                        
                        <div className="mt-2">
                        <Button className="btn-outline-success btp" style={{marginRight:"10px"}}>
                            Add Notes
                        </Button>
                        <Button className="btn-outline-success btp">
                            Save Clips
                        </Button>
                        </div>

                    </Card.Text>
                    </Card.Body>
                    <Card.Footer className="cf">
                    <small className="text-muted">Motion @Airline Shopping Center</small>
                    </Card.Footer>
                </Card>
            </Col>
            </Row>
            {/* second  */}
            <Row className="mt-2">
            {/* 4th Player */}
            <Col className="col-lg-4 col-md-6 col-sm-12 col-xs-12">
                <Card>
                    {/* <Player1/> */}
                    <Card.Body className="cardBody">
                    <Card.Title>Rosenburgh</Card.Title>
                    <Card.Text>
                        
                        <div className="mt-2">
                        <Button className="btn-outline-success btp" style={{marginRight:"10px"}}>
                            Add Notes
                        </Button>
                        <Button className="btn-outline-success btp">
                            Save Clips
                        </Button>
                        </div>

                    </Card.Text>
                    </Card.Body>
                    <Card.Footer className="cf">
                    <small className="text-muted">Motion @Airline Shopping Center</small>
                    </Card.Footer>
                </Card>
            </Col>
            {/* 5th Player */}
            <Col className="col-lg-4 col-md-6 col-sm-12 col-xs-12">
                <Card>
                    {/* <Player2/> */}
                    <Card.Body className="cardBody">
                    <Card.Title>Mentor Ave </Card.Title>
                    <Card.Text>
                        
                        <div className="mt-2">
                        <Button className="btn-outline-success btp" style={{marginRight:"10px"}}>
                            Add Notes
                        </Button>
                        <Button className="btn-outline-success btp">
                            Save Clips
                        </Button>
                        </div>

                    </Card.Text>
                    </Card.Body>
                    <Card.Footer className="cf">
                    <small className="text-muted">Motion @Airline Shopping Center</small>
                    </Card.Footer>
                </Card>
            </Col>
            {/* 6th Player */}
            <Col className="col-lg-4 col-md-6 col-sm-12 col-xs-12">
                <Card>
                    {/* <Player/> */}
                    <Card.Body className="cardBody">
                    <Card.Title>Rosenburgh</Card.Title>
                    <Card.Text>
                        
                        <div className="mt-2">
                        <Button className="btn-outline-success btp" style={{marginRight:"10px"}}>
                            Add Notes
                        </Button>
                        <Button className="btn-outline-success btp">
                            Save Clips
                        </Button>
                        </div>

                    </Card.Text>
                    </Card.Body>
                    <Card.Footer className="cf">
                    <small className="text-muted">Motion @Airline Shopping Center</small>
                    </Card.Footer>
                </Card>
            </Col>
            </Row>
            {/* third */}
            <Row className="mt-2">
                {/* 7th Player */}
            <Col className="col-lg-4 col-md-6 col-sm-12 col-xs-12">
                <Card>
                    {/* <Player1/> */}
                    <Card.Body className="cardBody">
                    <Card.Title>Rundberg</Card.Title>
                    <Card.Text>
                        
                        <div className="mt-2">
                        <Button className="btn-outline-success btp" style={{marginRight:"10px"}}>
                            Add Notes
                        </Button>
                        <Button className="btn-outline-success btp">
                            Save Clips
                        </Button>
                        </div>

                    </Card.Text>
                    </Card.Body>
                    <Card.Footer className="cf">
                    <small className="text-muted">Motion @Airline Shopping Center</small>
                    </Card.Footer>
                </Card>
            </Col>
            {/* 8Th Player */}
            <Col className="col-lg-4 col-md-6 col-sm-12 col-xs-12">
                <Card>
                    {/* <Player2/> */}
                    <Card.Body className="cardBody">
                    <Card.Title>Tillmans Corner</Card.Title>
                    <Card.Text>
                        
                        <div className="mt-2">
                        <Button className="btn-outline-success btp" style={{marginRight:"10px"}}>
                            Add Notes
                        </Button>
                        <Button className="btn-outline-success btp">
                            Save Clips
                        </Button>
                        </div>

                    </Card.Text>
                    </Card.Body>
                    <Card.Footer className="cf">
                    <small className="text-muted">Motion @Airline Shopping Center</small>
                    </Card.Footer>
                </Card>
            </Col>
            {/* 9Th Player */}
            <Col className="col-lg-4 col-md-6 col-sm-12 col-xs-12">
                <Card>
                    {/* <Player/> */}
                    <Card.Body className="cardBody">
                    <Card.Title>Perkins Rd</Card.Title>
                    <Card.Text>
                        
                        <div className="mt-2">
                        <Button className="btn-outline-success btp" style={{marginRight:"10px"}}>
                            Add Notes
                        </Button>
                        <Button className="btn-outline-success btp">
                            Save Clips
                        </Button>
                        </div>

                    </Card.Text>
                    </Card.Body>
                    <Card.Footer className="cf">
                    <small className="text-muted">Motion @Airline Shopping Center</small>
                    </Card.Footer>
                </Card>
            </Col>
            </Row>     
        </div>
        </>
    )
}
