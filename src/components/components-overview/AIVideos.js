import {Row,Col} from "shards-react";
import Button from 'react-bootstrap/Button'
import "../../views/Button.css"
import Card from 'react-bootstrap/Card' 
import '../../views/scrollbar.css';

import Player from "./HLS_js/Hls_Js.js"
import Player1 from "./HLS_js/Hls_Js1.js"
import Player2 from "./HLS_js/Hls_Js2.js"

const Aivideos = () => {
    return (
        <div>
            <div className="scrollbar scrollbar-juicy-peach">
            <Row className="mt-2">
            <Col className="col-lg-4 col-md-6 col-sm-12 col-xs-12 ">
            <Card className="bgrn">
                {/* <Player/> */}
            <Card.Body className="cardBody" >
            <Card.Title className="bgrn">WestAve</Card.Title>
            <Card.Text>
                <div className="mt-2">
                <Button className="btn-outline-success btp" style={{marginRight:"10px"}}>
                    Add Notes
                </Button>
                <Button className="btn-outline-success btp">
                    Save Clips
                </Button>
                </div>

            </Card.Text>
            </Card.Body>
            <Card.Footer className="cf">
            <small className="text-muted">Motion @Airline Shopping Center</small>
            </Card.Footer>
            </Card>
            </Col>
            <Col className="col-lg-4 col-md-6 col-sm-12 col-xs-12">
            <Card className="bgrn">
                {/* <Player1/> */}
             <Card.Body className="cardBody">
            <Card.Title className="bgrn">11th St Niles</Card.Title>
            <Card.Text>
                
                <div className="mt-2">
                <Button className="btn-outline-success btp" style={{marginRight:"10px"}}>
                    Add Notes
                </Button>
                <Button className="btn-outline-success btp">
                    Save Clips
                </Button>
                </div>

            </Card.Text>
            </Card.Body>
            <Card.Footer className="cf">
            <small className="text-muted">Motion @Airline Shopping Center</small>
            </Card.Footer>
            </Card>
            </Col>
            <Col className="col-lg-4 col-md-6 col-sm-12 col-xs-12">
            <Card className="bgrn">
            {/* <Player2/> */}
            <Card.Body className="cardBody">
            <Card.Title className="bgrn">New Philadelphia</Card.Title>
            <Card.Text>
                
                <div className="mt-2">
                <Button className="btn-outline-success btp" style={{marginRight:"10px"}}>
                    Add Notes
                </Button>
                <Button className="btn-outline-success btp">
                    Save Clips
                </Button>
                </div>

            </Card.Text>
            </Card.Body>
            <Card.Footer className="cf">
            <small className="text-muted">Motion @Airline Shopping Center</small>
            </Card.Footer>
            </Card>
            </Col>
            </Row>
            {/* second  */}
            <Row className="mt-2">
            <Col className="col-lg-4 col-md-6 col-sm-12 col-xs-12 ">
            <Card className="bgrn">
            {/* <Player/> */}
            <Card.Body className="cardBody" >
            <Card.Title className="bgrn">Medina</Card.Title>
            <Card.Text>
                <div className="mt-2">
                <Button className="btn-outline-success btp" style={{marginRight:"10px"}}>
                    Add Notes
                </Button>
                <Button className="btn-outline-success btp">
                    Save Clips
                </Button>
                </div>

            </Card.Text>
            </Card.Body>
            <Card.Footer className="cf">
            <small className="text-muted">Motion @Airline Shopping Center</small>
            </Card.Footer>
            </Card>
            </Col>
            <Col className="col-lg-4 col-md-6 col-sm-12 col-xs-12">
            <Card className="bgrn">
                {/* <Player1/> */}
            <Card.Body className="cardBody">
            <Card.Title className="bgrn">uvalde</Card.Title>
            <Card.Text>
                
                <div className="mt-2">
                <Button className="btn-outline-success btp" style={{marginRight:"10px"}}>
                    Add Notes
                </Button>
                <Button className="btn-outline-success btp">
                    Save Clips
                </Button>
                </div>

            </Card.Text>
            </Card.Body>
            <Card.Footer className="cf">
            <small className="text-muted">Motion @Airline Shopping Center</small>
            </Card.Footer>
            </Card>
            </Col>
            <Col className="col-lg-4 col-md-6 col-sm-12 col-xs-12">
            <Card className="bgrn">
            {/* <Player/> */}
            <Card.Body className="cardBody">
            <Card.Title className="bgrn">Stow</Card.Title>
            <Card.Text>
                
                <div className="mt-2">
                <Button className="btn-outline-success btp" style={{marginRight:"10px"}}>
                    Add Notes
                </Button>
                <Button className="btn-outline-success btp">
                    Save Clips
                </Button>
                </div>

            </Card.Text>
            </Card.Body>
            <Card.Footer className="cf">
            <small className="text-muted">Motion @Airline Shopping Center</small>
            </Card.Footer>
            </Card>
            </Col>
            </Row>
        </div>
            
        </div>
    )
}
export default Aivideos;
