import { Bar } from "react-chartjs-2";
const Graph = ({items,dates_int,marketdatas}) =>{
    const dates_integer=dates_int
    const selects=marketdatas
    const markets=(items,dates_integer,selects)=> {
    
        let dates=[]
        let date=items.map(item=>item.date)
        let market=items.map(item=> item.market_name)
        // console.log("Data And Market",date,market)
        for(let i in (date,market,items)) {
            // console.log(date,market)
          if (date[i]===dates_integer && market[i]===selects) {
            dates.push(items[i])
            console.log("what is dates",dates)
          }
        }
        return dates;
      }
     
      const data=markets(items,dates_integer,selects)
    //   console.log("What is data",data)
      const ECount=data.map((trafic)=>trafic.Expected_Count)
      const SName=data.map((sn)=>sn.Store_name)
      const LCount=data.map((lc)=>lc.Lower_count)
      const UCount=data.map((uc)=>uc.Upper_count)
    return(
        <div>
        <Bar
            data={{
                labels:SName,
                datasets:[
                    {
                        label:"Expected Count",
                        data:ECount,
                        backgroundColor:["burlywood"
                            // 'rgba(255, 99, 132, 0.2)',
                            // 'rgba(255, 159, 64, 0.2)',
                            // 'rgba(255, 205, 86, 0.2)',
                            // 'rgba(75, 192, 192, 0.2)',
                            // 'rgba(54, 162, 235, 0.2)',
                            // 'rgba(153, 102, 255, 0.2)',
                            // 'rgba(201, 203, 207, 0.2)'
                        ],
                        borderColor:["chocolate"
                            // 'rgb(255, 99, 132)',
                            // 'rgb(255, 159, 64)',
                            // 'rgb(255, 205, 86)',
                            // 'rgb(75, 192, 192)',
                            // 'rgb(54, 162, 235)',
                            // 'rgb(153, 102, 255)',
                            // 'rgb(201, 203, 207)'
                        ],
                        borderWidth:4,
                    },
                    {
                    label:"Lower Count",
                    data:LCount,
                    backgroundColor:["  #E69A8DFF	 "
                        // 'rgba(255, 99, 132, 0.2)',
                        // 'rgba(255, 159, 64, 0.2)',
                        // 'rgba(255, 205, 86, 0.2)',
                        // 'rgba(75, 192, 192, 0.2)',
                        // 'rgba(54, 162, 235, 0.2)',
                        // 'rgba(153, 102, 255, 0.2)',
                        // 'rgba(201, 203, 207, 0.2)'
                    ],
                    borderColor:["#5F4B8BFF"
                        // 'rgb(255, 99, 132)',
                        // 'rgb(255, 159, 64)',
                        // 'rgb(255, 205, 86)',
                        // 'rgb(75, 192, 192)',
                        // 'rgb(54, 162, 235)',
                        // 'rgb(153, 102, 255)',
                        // 'rgb(201, 203, 207)'
                    ],
                    borderWidth:4,
                },
                {
                    label:"Upper Count",
                    data:UCount,
                    backgroundColor:[" #ADEFD1FF"
                        // 'rgba(255, 99, 132, 0.2)',
                        // 'rgba(255, 159, 64, 0.2)',
                        // 'rgba(255, 205, 86, 0.2)',
                        // 'rgba(75, 192, 192, 0.2)',
                        // 'rgba(54, 162, 235, 0.2)',
                        // 'rgba(153, 102, 255, 0.2)',
                        // 'rgba(201, 203, 207, 0.2)'
                    ],
                    borderColor:["#00203FFF",
                    //     'rgb(255, 99, 132)',
                    //     'rgb(255, 159, 64)',
                    //     'rgb(255, 205, 86)',
                    //     'rgb(75, 192, 192)',
                    //     'rgb(54, 162, 235)',
                    //     'rgb(153, 102, 255)',
                    //     'rgb(201, 203, 207)'
                    ],
                    borderWidth:4,
                }
                ],
            }
        }

            width="100%"
            height="30%"
           
            />
          </div>

    )
}
export default Graph;