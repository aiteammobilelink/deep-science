import React from "react";
import {
  Container,
  Row,
  Col,
} from "shards-react";
import Button from 'react-bootstrap/Button'
import "../../Button.css"
// import VideoCard from "./VideoCard.js"
import VideoCard from '../../../components/components-overview/VideoCard'
import CameraPlayer from "../../../components/components-overview/cameraPlayer";


const Clips = () => (
  
  <div>
    <Container fluid>
      <Row>
        <Col>
        <div style={{background:"white"}}><h3>Clips</h3></div>
        </Col>
      </Row>
      <Row>
        <Col className="col-lg-12 col-md-6 col-sm-3">
          <VideoCard />
        </Col>
      </Row>
      <Row>
        <Col className="col-lg-12 col-md-12 col-sm-12 center">
            <Button className="checkfornew">Check for new updates</Button>
        </Col>
      </Row>
      <Row>
        <Col className="col-lg-12 col-md-6 col-sm-3">
          <CameraPlayer />
        </Col>
      </Row>
    </Container>
  </div>
);

export default Clips;
