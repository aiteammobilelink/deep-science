

import React from "react";
import {
  Container,
  Row,
  Col,
  Form, 
  FormInput
} from "shards-react";
import '../../../assets/quill.css'
import '../../bp.css';
import PageTitle from "../../../components/common/PageTitle";
import AccordianComponent from '../../AccordianComponent'
import VideoCard from '../../../components/components-overview/VideoCard'
import CameraPlayer from "../../../components/components-overview/cameraPlayer"
class Events extends React.Component {
  render() {
    return (
      <Container fluid className="main-content-container px-4">
        {/* Page Header */}
        <Row noGutters className="page-header py-4">
          <Col className="col-lg-4 col-md-6 col-sm-6 col-xl-6">
          <PageTitle sm="4" title="All Events" className="text-sm-left" />
          </Col>
          <Col className="col-lg-8 col-md-6 col-sm-6 col-xl-6 d-flex justify-content-end">
            <div className="d-flex">
            <span class="material-icons" style={{fontSize:"20px", marginRight:"5px"}}>share</span>
            <p className="ml-1" style={{fontSize:"20px", color:"grey",fontWeight:"400",fontFamily:"sans-serif" , marginRight:"10px"}}>Share</p>
            </div>
            <div className="d-flex ml-4">
            <span class="material-icons" style={{fontSize:"20px", marginRight:"5px"}}>star_outline</span>
            <p className="ml-1" style={{fontSize:"20px", color:"grey", fontWeight:"400",fontFamily:"sans-serif", marginRight:"10px"}}>Favourite</p>
            </div>
          </Col>
        </Row>
        <Row>
        <Col className=" col-lg-6">
      <Form className="add-new-post">
      <FormInput size="lg" className="mb-3 round" placeholder="#Add a Search Item" 
      />
      </Form>
  </Col>
        </Row>
        {/* accodian  */}
        <Row>
          <Col className="col-lg-12 col-md-12 col-sm-12 col-xl-12 mb-3">
            <AccordianComponent />
          </Col>
        </Row>
        <Row>
          <Col>
          <VideoCard />
          </Col>
        </Row>
        <Row className="mb-2">
          <Col className="col-lg-12 col-md-12 col-sm-12 col-xl-12">
            <CameraPlayer />
          </Col>
        </Row>
      </Container>
    );
  }
}

export default Events;
