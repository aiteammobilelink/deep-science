import React from 'react'
import {
    Row,
    Col,
    
} from "shards-react";
import Button from 'react-bootstrap/Button'
import "../../../views/Button.css"
import Card from 'react-bootstrap/Card'
// import ScrollArea from 'react-scrollbar'; 
// import '../views/scrollbar.css';

import Videojs from "../../../components/components-overview/videoPlayer";
import {Medina,uvalde,Stow,srf9} from "../../../components/components-overview/file.js"


const SFvideos = () => {
    return (
        <div>
            <div className="backGround">
            <Row>
            <Col className="col-lg-6 col-md-6 col-sm-12 col-xs-12 ">
            <Card className="bgrn">
            <Videojs video={Stow} />
            {/* <Reabtplayer min-height="100%" width="100%" url='rtsp://surv:zara0786@71.73.2.198:5445/Stream1/Channel=1' playing = {true} /> */}
            
            {/* <ReactPlayer min-height="100%" width="100%" src={change} playing = {true} /> */}
            <Card.Body className="cardBody" >
            <Card.Title className="bgrn">WestAve</Card.Title>
            <Card.Text>
                <div className="mt-2">
                <Button className="btn-outline-success btp" style={{marginRight:"10px"}}>
                    Add Notes
                </Button>
                <Button className="btn-outline-success btp">
                    Save Clips
                </Button>
                </div>

            </Card.Text>
            </Card.Body>
            <Card.Footer className="cf">
            <small className="text-muted">Motion @Airline Shopping Center</small>
            </Card.Footer>
            </Card>
            </Col>
            <Col className="col-lg-6 col-md-6 col-sm-12 col-xs-12">
            <Card className="bgrn">
              <Videojs video={srf9}/>
            {/* <Reabtplayer min-height="100%" width="100%" url='https://www.youtube.com/watch?v=ZZ4B0QUHuNc' playing = {true}  /> */}
            <Card.Body className="cardBody">
            <Card.Title className="bgrn">Perkins Rd</Card.Title>
            <Card.Text>
                
                <div className="mt-2">
                <Button className="btn-outline-success btp" style={{marginRight:"10px"}}>
                    Add Notes
                </Button>
                <Button className="btn-outline-success btp">
                    Save Clips
                </Button>
                </div>

            </Card.Text>
            </Card.Body>
            <Card.Footer className="cf">
            <small className="text-muted">Motion @Airline Shopping Center</small>
            </Card.Footer>
            </Card>
            </Col>
            </Row>
            {/* second  */}
            <Row className="mt-2">
            <Col className="col-lg-6 col-md-6 col-sm-12 col-xs-12 ">
            <Card className="bgrn">
            <Videojs video={Medina} />
            {/* <Reabtplayer min-height="100%" width="100%" url='rtsp://surv:zara0786@71.73.2.198:5445/Stream1/Channel=1' playing = {true} /> */}
            
            {/* <ReactPlayer min-height="100%" width="100%" src={change} playing = {true} /> */}
            <Card.Body className="cardBody" >
            <Card.Title className="bgrn">Medina</Card.Title>
            <Card.Text>
                <div className="mt-2">
                <Button className="btn-outline-success btp" style={{marginRight:"10px"}}>
                    Add Notes
                </Button>
                <Button className="btn-outline-success btp">
                    Save Clips
                </Button>
                </div>

            </Card.Text>
            </Card.Body>
            <Card.Footer className="cf">
            <small className="text-muted">Motion @Airline Shopping Center</small>
            </Card.Footer>
            </Card>
            </Col>
            <Col className="col-lg-6 col-md-6 col-sm-12 col-xs-12">
            <Card className="bgrn">
              <Videojs video={uvalde}/>
            {/* <Reabtplayer min-height="100%" width="100%" url='https://www.youtube.com/watch?v=ZZ4B0QUHuNc' playing = {true}  /> */}
            <Card.Body className="cardBody">
            <Card.Title className="bgrn">uvalde</Card.Title>
            <Card.Text>
                
                <div className="mt-2">
                <Button className="btn-outline-success btp" style={{marginRight:"10px"}}>
                    Add Notes
                </Button>
                <Button className="btn-outline-success btp">
                    Save Clips
                </Button>
                </div>

            </Card.Text>
            </Card.Body>
            <Card.Footer className="cf">
            <small className="text-muted">Motion @Airline Shopping Center</small>
            </Card.Footer>
            </Card>
            </Col>
            </Row>
        </div>
            
        </div>
    )
}
export default SFvideos;
