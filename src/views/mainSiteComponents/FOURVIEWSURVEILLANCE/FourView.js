import React from 'react'
import {
    Row,
    Col,
} from "shards-react";
import Button from 'react-bootstrap/Button'
import "../../../views/Button.css"
import Card from 'react-bootstrap/Card'
import Videojs from "../../../components/components-overview/videoPlayer";
import {thStNiles,WestAve,MedinaAI,uvaldeAI} from "../../../components/components-overview/file.js"


const Fvideos = () => {
    return (
        <div>
            <div className="backGround">
            <Row className="mt-2">
            <Col className="col-lg-6 col-md-6 col-sm-12 col-xs-12 ">
            <Card className="bgrn">
            <Videojs video={WestAve} />
            <Card.Body className="cardBody" >
            <Card.Title className="bgrn">WestAve</Card.Title>
            <Card.Text>
                <div className="mt-2">
                <Button className="btn-outline-success btp" style={{marginRight:"10px"}}>
                    Add Notes
                </Button>
                <Button className="btn-outline-success btp">
                    Save Clips
                </Button>
                </div>

            </Card.Text>
            </Card.Body>
            <Card.Footer className="cf">
            <small className="text-muted">Motion @Airline Shopping Center</small>
            </Card.Footer>
            </Card>
            </Col>
            <Col className="col-lg-6 col-md-6 col-sm-12 col-xs-12">
            <Card className="bgrn">
              <Videojs video={thStNiles}/>
            <Card.Body className="cardBody">
            <Card.Title className="bgrn">11th St Niles</Card.Title>
            <Card.Text>
                
                <div className="mt-2">
                <Button className="btn-outline-success btp" style={{marginRight:"10px"}}>
                    Add Notes
                </Button>
                <Button className="btn-outline-success btp">
                    Save Clips
                </Button>
                </div>

            </Card.Text>
            </Card.Body>
            <Card.Footer className="cf">
            <small className="text-muted">Motion @Airline Shopping Center</small>
            </Card.Footer>
            </Card>
            </Col>
            </Row>
            <Row className="mt-2">
            <Col className="col-lg-6 col-md-6 col-sm-12 col-xs-12 ">
            <Card className="bgrn">
            <Videojs video={MedinaAI} />
            <Card.Body className="cardBody" >
            <Card.Title className="bgrn">Medina</Card.Title>
            <Card.Text>
                <div className="mt-2">
                <Button className="btn-outline-success btp" style={{marginRight:"10px"}}>
                    Add Notes
                </Button>
                <Button className="btn-outline-success btp">
                    Save Clips
                </Button>
                </div>

            </Card.Text>
            </Card.Body>
            <Card.Footer className="cf">
            <small className="text-muted">Motion @Airline Shopping Center</small>
            </Card.Footer>
            </Card>
            </Col>
            <Col className="col-lg-6 col-md-6 col-sm-12 col-xs-12">
            <Card className="bgrn">
              <Videojs video={uvaldeAI}/>
            <Card.Body className="cardBody">
            <Card.Title className="bgrn">uvalde</Card.Title>
            <Card.Text>
                
                <div className="mt-2">
                <Button className="btn-outline-success btp" style={{marginRight:"10px"}}>
                    Add Notes
                </Button>
                <Button className="btn-outline-success btp">
                    Save Clips
                </Button>
                </div>

            </Card.Text>
            </Card.Body>
            <Card.Footer className="cf">
            <small className="text-muted">Motion @Airline Shopping Center</small>
            </Card.Footer>
            </Card>
            </Col>
            </Row>
        </div>
            
        </div>
    )
}
export default Fvideos;
