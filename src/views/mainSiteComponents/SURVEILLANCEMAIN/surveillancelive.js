import { Container, Row, Col } from "shards-react";
import Secondnav from '../../../components/add-new-post/secondnav';
import '../../../components/add-new-post/video.css';
import {Button } from "shards-react";
import {Link} from 'react-router-dom'
import MediaPlayer from '../../../components/components-overview/secondCard'
import SideToggle from "../../video_sidetoggle";
import Addingrow from "../../addingrow";
function VideosComponent(){
  return(
  <Container fluid className="main-content-container" style={{background:'#4A4746'}}>
    {/* Page Header */}
    <Row noGutters className="page-header py-3">
    <Secondnav />
      {/* <PageTitle sm="4" title="Add New Post" subtitle="Blog Posts" className="text-sm-left" /> */}
    </Row>
    <Row>
      {/* Editor */}
      <Col className="col-lg-6 col-md-6 col-sm-6">
        <div className="d-flex justify-content-start ctp">
        <h5>Camera to play</h5>    
        <Link to="/SurveillanceView"><Button>4</Button></Link>
         <Link to="/SEView"> <Button>8</Button></Link>
          </div>
          </Col>
      <Col className="col-lg-6 col-md-6 col-sm-12 d-flex justify-content-end">
        <SideToggle />
      </Col>
      <Col className="col-lg-12 col-md-6 col-sm-12">
          <Addingrow />
      </Col>
      {/* Sidebar Widgets */}
      <Col lg="12" md="12" sm="12" xs="12" xl="12">
     <MediaPlayer />
        {/* <SidebarActions />
        <SidebarCategories /> */}
      </Col>

    </Row>
  </Container>
);
}
export default VideosComponent;
