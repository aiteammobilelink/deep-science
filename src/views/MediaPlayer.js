import React from 'react'
import {
    Container,
    Row,
    Col,
    // Card,
    CardBody,
    CardFooter,
    Badge,
    // Button,
    Form, 
    FormInput
} from "shards-react";
import Select from 'react-select'
import Button from 'react-bootstrap/Button'
import "./Button.css"
import Card from 'react-bootstrap/Card'
import CardGroup from 'react-bootstrap/CardGroup'
import ReactPlayer from 'react-player';
import HoverVideoPlayer from 'react-hover-video-player';
import ScrollArea from 'react-scrollbar'; 
import './scrollbar.css';
export default function MediaPlayer() {
    return (
            
        <div className="scrollbar scrollbar-juicy-peach">
            <Row className="mt-2">
            <Col className="col-lg-4 col-md-6 col-sm-12 col-xs-12 ">
            <Card>
            {/* <ReactPlayer min-height="100%" width="100%" url='rtsp://surv:zara0786@71.73.2.198:5445/Stream1/Channel=1' playing = {true} /> */}
            <ReactPlayer min-height="100%" width="100%" url='https://www.youtube.com/watch?v=2ePf9rue1Ao' playing = {true} />
            <Card.Body className="cardBody" >
            <Card.Title>Murphy</Card.Title>
            <Card.Text>
                text below as a natural lead-in to additional
                content.{' '} <br />
                <div className="mt-2">
                <Button className="btn-success" style={{marginRight:"10px"}}>
                    Add Notes
                </Button>
                <Button className="btn-success">
                    Save Clips
                </Button>
                </div>

            </Card.Text>
            </Card.Body>
            <Card.Footer style={{color:'white',background:"#2d2d2d"}}>
            <small className="text-muted">Motion @Airline Shopping Center</small>
            </Card.Footer>
            </Card>
            </Col>
            <Col className="col-lg-4 col-md-6 col-sm-12 col-xs-12">
            <Card>
            <ReactPlayer min-height="100%" width="100%" url='https://www.youtube.com/watch?v=ZZ4B0QUHuNc' playing = {true}  />
            <Card.Body className="cardBody">
            <Card.Title>Murphy</Card.Title>
            <Card.Text>
                text below as a natural lead-in to additional
                content.{' '} <br />
                <div className="mt-2">
                <Button className="btn-success" style={{marginRight:"10px"}}>
                    Add Notes
                </Button>
                <Button className="btn-success">
                    Save Clips
                </Button>
                </div>

            </Card.Text>
            </Card.Body>
            <Card.Footer style={{color:'white',background:"#2d2d2d"}}>
            <small className="text-muted">Motion @Airline Shopping Center</small>
            </Card.Footer>
            </Card>
            </Col>
            <Col className="col-lg-4 col-md-6 col-sm-12 col-xs-12">
            <Card>
            <ReactPlayer min-height="100%" width="100%" url='https://www.youtube.com/watch?v=rHux0gMZ3Eg' playing = {true} />
            <Card.Body className="cardBody">
            <Card.Title>Murphy</Card.Title>
            <Card.Text>
                text below as a natural lead-in to additional
                content.{' '} <br />
                <div className="mt-2">
                <Button className="btn-success" style={{marginRight:"10px"}}>
                    Add Notes
                </Button>
                <Button className="btn-success">
                    Save Clips
                </Button>
                </div>

            </Card.Text>
            </Card.Body>
            <Card.Footer style={{color:'white',background:"#2d2d2d"}}>
            <small className="text-muted">Motion @Airline Shopping Center</small>
            </Card.Footer>
            </Card>
            </Col>
            </Row>
            {/* second  */}
            <Row className="mt-2">
            <Col className="col-lg-4 col-md-6 col-sm-12 col-xs-12">
            <Card>
            <ReactPlayer min-height="100%" width="100%" url='https://www.youtube.com/watch?v=UmljXZIypDc' playing = {true} />
            <Card.Body className="cardBody">
            <Card.Title>Murphy</Card.Title>
            <Card.Text>
                text below as a natural lead-in to additional
                content.{' '} <br />
                <div className="mt-2">
                <Button className="btn-success" style={{marginRight:"10px"}}>
                    Add Notes
                </Button>
                <Button className="btn-success">
                    Save Clips
                </Button>
                </div>

            </Card.Text>
            </Card.Body>
            <Card.Footer style={{color:'white',background:"#2d2d2d"}}>
            <small className="text-muted">Motion @Airline Shopping Center</small>
            </Card.Footer>
            </Card>
            </Col>
            <Col className="col-lg-4 col-md-6 col-sm-12 col-xs-12">
            <Card>
            <ReactPlayer min-height="100%" width="100%" url='https://www.youtube.com/watch?v=b093aqAZiPU' playing = {true} />
            <Card.Body className="cardBody">
            <Card.Title>Murphy</Card.Title>
            <Card.Text>
                text below as a natural lead-in to additional
                content.{' '} <br />
                <div className="mt-2">
                <Button className="btn-success" style={{marginRight:"10px"}}>
                    Add Notes
                </Button>
                <Button className="btn-success">
                    Save Clips
                </Button>
                </div>

            </Card.Text>
            </Card.Body>
            <Card.Footer style={{color:'white',background:"#2d2d2d"}}>
            <small className="text-muted">Motion @Airline Shopping Center</small>
            </Card.Footer>
            </Card>
            </Col>
            <Col className="col-lg-4 col-md-6 col-sm-12 col-xs-12">
            <Card>
            <ReactPlayer min-height="100%" width="100%" url='https://www.youtube.com/watch?v=s0dMTAQM4cw' playing = {true} />
            <Card.Body className="cardBody">
            <Card.Title>Murphy</Card.Title>
            <Card.Text>
                text below as a natural lead-in to additional
                content.{' '} <br />
                <div className="mt-2">
                <Button className="btn-success" style={{marginRight:"10px"}}>
                    Add Notes
                </Button>
                <Button className="btn-success">
                    Save Clips
                </Button>
                </div>

            </Card.Text>
            </Card.Body>
            <Card.Footer style={{color:'white',background:"#2d2d2d"}}>
            <small className="text-muted">Motion @Airline Shopping Center</small>
            </Card.Footer>
            </Card>
            </Col>
            </Row>
            {/* third */}
            <Row className="mt-2">
            <Col className="col-lg-4 col-md-6 col-sm-12 col-xs-12">
            <Card>
            <ReactPlayer min-height="100%" width="100%" url='https://www.youtube.com/watch?v=JMUxmLyrhSk' playing = {true} />
            <Card.Body className="cardBody">
            <Card.Title>Murphy</Card.Title>
            <Card.Text>
                text below as a natural lead-in to additional
                content.{' '} <br />
                <div className="mt-2">
                <Button className="btn-success" style={{marginRight:"10px"}}>
                    Add Notes
                </Button>
                <Button className="btn-success">
                    Save Clips
                </Button>
                </div>

            </Card.Text>
            </Card.Body>
            <Card.Footer style={{color:'white',background:"#2d2d2d"}}>
            <small className="text-muted">Motion @Airline Shopping Center</small>
            </Card.Footer>
            </Card>
            </Col>
            <Col className="col-lg-4 col-md-6 col-sm-12 col-xs-12">
            <Card>
            <ReactPlayer min-height="100%" width="100%" url='https://www.youtube.com/watch?v=5B3Wn6Wo5CU' playing = {true} />
            <Card.Body className="cardBody">
            <Card.Title>Murphy</Card.Title>
            <Card.Text>
                text below as a natural lead-in to additional
                content.{' '} <br />
                <div className="mt-2">
                <Button className="btn-success" style={{marginRight:"10px"}}>
                    Add Notes
                </Button>
                <Button className="btn-success">
                    Save Clips
                </Button>
                </div>

            </Card.Text>
            </Card.Body>
            <Card.Footer style={{color:'white',background:"#2d2d2d"}}>
            <small className="text-muted">Motion @Airline Shopping Center</small>
            </Card.Footer>
            </Card>
            </Col>
            <Col className="col-lg-4 col-md-6 col-sm-12 col-xs-12">
            <Card>
            <ReactPlayer min-height="100%" width="100%" url='https://www.youtube.com/watch?v=Dpzx2DwwWnE' playing = {true} />
            <Card.Body className="cardBody">
            <Card.Title>Murphy</Card.Title>
            <Card.Text>
                text below as a natural lead-in to additional
                content.{' '} <br />
                <div className="mt-2">
                <Button className="btn-success" style={{marginRight:"10px"}}>
                    Add Notes
                </Button>
                <Button className="btn-success">
                    Save Clips
                </Button>
                </div>

            </Card.Text>
            </Card.Body>
            <Card.Footer style={{color:'white',background:"#2d2d2d"}}>
            <small className="text-muted">Motion @Airline Shopping Center</small>
            </Card.Footer>
            </Card>
            </Col>
            </Row>
            
            
            {/* <Row className="mt-3">
            <Col className="col-lg-4 col-md-6 col-sm-12 col-xs-12 col-md-4 col-sm-4 col-xl-4">
            <ReactPlayer min-height="100%"  width="100%" url='https://www.youtube.com/embed/muuK4SpRR5M' playing = {true} />
            </Col>
            <Col className="col-lg-4 col-md-4 col-sm-4 col-xl-4">
            <ReactPlayer min-height="100%"  width="100%" url='https://www.youtube.com/watch?v=ZZ4B0QUHuNc' playing = {true} />
            </Col>
            <Col className="col-lg-4 col-md-4 col-sm-4 col-xl-4">
            <ReactPlayer min-height="100%"  width="100%" url='https://www.youtube.com/watch?v=rfscVS0vtbw' playing = {true} />
            </Col>
    </Row>*/}       
        </div>
    )
}
