import ReactPlayer from 'react-player';
import {
    Container,
    Row,
    Col,
    CardHeader,
    ListGroup,
    ListGroupItem,
    Form,
  } from "shards-react";

import HoverVideoPlayer from 'react-hover-video-player';
const VideoCard = () => {
    return (
      <HoverVideoPlayer
        videoSrc="path-to/your-video.mp4"
        pausedOverlay={
          <img
            src="thumbnail-image.jpg"
            alt=""
            style={{
              // Make the image expand to cover the video's dimensions
              width: "100%",
              height: "100%",
              objectFit: "cover",
            }}
          />
        }
        loadingOverlay={
          <div className="loading-overlay">
            <div className="loading-spinner" />
          </div>
        }
      />
    );
  }
export default VideoCard;