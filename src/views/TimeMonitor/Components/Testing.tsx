import React from 'react';
import {
  Chart as ChartJS,
  LinearScale,
  CategoryScale,
  BarElement,
  PointElement,
  LineElement,
  Legend,
  Tooltip,
} from 'chart.js';
import { Chart } from 'react-chartjs-2';
import faker from 'faker';
import { List } from 'reselect/es/types';

ChartJS.register(
  LinearScale,
  CategoryScale,
  BarElement,
  PointElement,
  LineElement,
  Legend,
  Tooltip
);

// ['January', 'February', 'March', 'April', 'May', 'June', 'July']
const Testing=(object:any)=> {
  
  console.log("In TSX File",object)
  let stores: any[]=[];
  for (let i in (object.store)){
    stores.push(object.store[i])
  }
  let openstore:any[]=[];
  for (let i in (object.open)){
    openstore.push(object.open[i])
  }
  let closestore:any[]=[];
  for (let i in (object.close)){
    closestore.push(object.close[i])
  }
  let timeline:any[]=[];
  for (let i in (object.lenth)){
    timeline.push(object.lenth[i])
  }
  let offtimeline:any[]=[];
  for (let i in (object.oflen)){
    offtimeline.push(object.oflen[i])
  }
  let latetime:any[]=[];
  for (let i in (object.late)){
    latetime.push(object.late[i])
  }
  let lateoptime:any[]=[];
  for (let i in (object.lateopentime)){
    lateoptime.push(object.lateopentime[i])
  }
  console.log("Late open intsx",lateoptime)
  
  console.log("What is Openstore in TSX File",timeline)
  const options = {
    scales: {
      y: {
        beginAtZero: false,
      },
      x: {
        beginAtZero: false,
      },
    },
  };
  const labels = stores;
  const data = {
  labels,
  datasets: [
    {
      type: 'line' as const,
      label: 'Open Store Time',
      borderColor: 'rgb(255, 99, 132)',
      borderWidth: 2,
      fill: false,
      data:timeline,
    },
    
    {
      type: 'bubble' as const,
      label: 'Early Open',
      backgroundColor: 'rgb(53, 162, 235)',
      data: openstore,
    },
    {
      type: 'line' as const,
      label: 'Store Close Time',
      backgroundColor: 'rgb(75, 192, 192)',
      data: offtimeline,
      borderColor: 'black',
      borderWidth: 2,
    },
    {
      type: 'bubble' as const,
      label: 'Early Close',
      backgroundColor: 'rgb(53, 162, 235)',
      data: closestore,
    },
    {
      type: 'bubble' as const,
      label: 'Late Close',
      backgroundColor: 'rgb(53, 162, 235)',
      data: latetime,
    },
    
    {
      type: 'bubble' as const,
      label: 'Late Open',
      backgroundColor: 'rgb(53, 162, 235)',
      data: lateoptime,
    },
    

    
  ],
};

  return <Chart options={options} type='bar' data={data} />;
}
export default Testing