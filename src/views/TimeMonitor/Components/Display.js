import React from 'react'
import { Container, Row, Col } from "shards-react";
import { useState} from "react";

import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";
import TableDataForTM from "../../TablesComponent/TablesDataForTM.js"
import '../../bp.css'
// import Graph1 from './Graph1.tsx';
// import Graph2 from './Graph2.js';
// import Testing from './Testing.js'
import Data from "./data"
const Display = ({fulldata,marketname}) => {
    const items=fulldata
    const [selects,setSelects]=useState("Houston");
    const [dates , setDates] = useState(new Date("2021-12-01"));
    const date_integer=(dates)=>{
      const dates_integer = new Intl.DateTimeFormat('en-US', { year: 'numeric',month:"2-digit",day: '2-digit', hour: '2-digit', minute: '2-digit', second: '2-digit'}).format(dates)
      const date_split= dates_integer.toString().split(" ")[0].split(",")[0]
      const dates_split=date_split.split("/")
      const dates_split_year=dates_split[2]
      const dates_split_month=dates_split[0]
      const dates_split_day=dates_split[1]
      const date=dates_split_year.concat("-",dates_split_month,"-",dates_split_day);

    //   console.log("in date integer",date)
      return date.toString()
    }
    const dates_in_integer=date_integer(dates)
    const dates_split=(dates)=>{
      const dates_split=dates.toString().split(" ")
      const dates_split_day=dates_split[0]
      const dates_split_month=dates_split[1]
      const dates_split_year=dates_split[3]
      const date=dates_split_month.concat("-",dates_split_day,"-",dates_split_year);
    //   console.log("int split funciton",date)
      return date
    }
  
    var dupilcates=items.map((market)=>market.market)
    let unique = new Set(dupilcates)
    let unique_values = [...unique]
    const optionItems = unique_values.map((item) =>
    <option key={item} >{item}</option>);
    const dates_integer = date_integer(dates);

  
    return (
        <Container fluid className="main-content-container mt-5">
    <Row>
        <Col className="col-lg mb-6">
          <h3 >{dates_split(dates)}</h3>
        </Col>
    </Row>
    <Row>
      <Col className="col-lg mb-3 d-flex">
      <span class="material-icons">filter_alt</span>
      <h6 className="mb-2" style={{fontSize:"20px", fontFamily:"sans-serif"}}> Filter</h6>
      &nbsp;&nbsp; 
      
      <span style={{marginRight:"5px"}}>
      <DatePicker className="datepickeroption" 
      // closeOnScroll={true}
      selected={dates}
      onChange={(date) => setDates(date)} 
      placeholderText="Select a date"
      />
      </span> 
        
        {/* {console.log(new Intl.DateTimeFormat('en-US', {year: 'numeric', month: '2-digit',day: '2-digit', hour: '2-digit', minute: '2-digit', second: '2-digit'}).format(dates))}  */}
      {/* {console.log(date_integer(dates))} */}
      <span className="mb-2" style={{marginRight:"5px"}}>
      <select className="selectsoption" value={selects} onChange={e=>setSelects(e.target.value)}>
      {optionItems}
      </select>
        </span>
      </Col>
      <Col className="col-lg">
        
      </Col>
    </Row>

    <Row>
        <Col lg={12} md={6} sm={12}>
        {/* {console.log("In to the display ",dates_in_integer,selects,items)} */}
    {/* <Graph1 items={items} dates_int={dates_in_integer} marketdatas={selects} marketnames={marketname}/> */}
      {/* <Testing items={items} dates_int={dates_in_integer} marketdatas={selects} marketnames={marketname}/> */}
      <Data items={items} dates_int={dates_in_integer} marketdatas={selects} marketnames={marketname} />
      </Col>
      {/* <Col lg={6} md={6} sm={12}>
        {/* {console.log("In to the display ",dates_in_integer,selects,items)} */}
    {/* <Graph2 items={items} dates_int={dates_in_integer} marketdatas={selects} marketnames={marketname}/> */}
      {/* </Col> */} 
      </Row>
      <Row>
      <Col lg="12" md="12" sm="12" className="mb-4">
      <TableDataForTM data={items}/>
      </Col>
    </Row>
  </Container>
    )
}

export default Display;
