
import { Container, Row, Col, Card, CardHeader, CardBody } from "shards-react";

import PageTitle from "../../components/common/PageTitle";

const TablesData = ({data}) =>  {
    // const 
  return(
    
<Container fluid className="main-content-container px-4">
    {/* Page Header */}
    <Row noGutters className="page-header py-4">
      <PageTitle sm="4" title="Deep Science AI" subtitle="Predictive Count" className="text-sm-left" />
    </Row>
    <Row>
      <Col>
        <Card small className="mb-4 overflow-hidden">
          <CardHeader className="bg-dark">
            <h6 className="m-0 text-white">Predictive Data </h6>
          </CardHeader>
          <CardBody className="bg-dark p-0 pb-3">
            <table className="table table-dark mb-0">
              <thead className="thead-dark">
                <tr>
                  <th scope="col" className="border-0">
                    StoreUID
                  </th>
                  <th scope="col" className="border-0">
                    Market
                  </th>
                  <th scope="col" className="border-0">
                    Location
                  </th>
                  <th scope="col" className="border-0">
                    Date
                  </th>
                  <th scope="col" className="border-0">
                  IncomingTraffic
                  </th>
                  <th scope="col" className="border-0">
                  Sale
                  </th>
                </tr>
              </thead>
              <tbody>
                {
                    data.map((item) => (
                        <tr key={item.StoreUID}>
                            <td>{item.StoreUID}</td>
                            <td>{item.Market}</td>
                            <td>{item.Location}</td>
                            <td>{item.Date}</td>
                            <td>{item.IncomingTraffic}</td>
                            <td>{item.Sale}</td>
                            <td/>
                        </tr>
                    ))
                    }
              </tbody>
            </table>
          
          </CardBody>
        </Card>
      </Col>
    </Row>
  </Container>
)
}; 
export default TablesData;