
import { Container, Row, Col, Card, CardHeader, CardBody } from "shards-react";

import PageTitle from "../../components/common/PageTitle";

const TableDataForUPL = ({data}) =>  {
    // const 
  return(
    
<Container fluid className="main-content-container px-4">
    {/* Page Header */}
    <Row noGutters className="page-header py-4">
      <PageTitle sm="4" title="Deep Science AI" subtitle="Traffic Count" className="text-sm-left" />
    </Row>
    <Row>
      <Col>
        <Card small className="mb-4 overflow-hidden">
          <CardHeader className="bg-dark">
            <h6 className="m-0 text-white">Traffic Count </h6>
          </CardHeader>
          <CardBody className="bg-dark p-0 pb-3">
            <table className="table table-dark mb-0">
              <thead className="thead-dark">
                <tr>
                  <th scope="col" className="border-0">
                    StoreUID
                  </th>
                  <th scope="col" className="border-0">
                    StoreName
                  </th>
                  <th scope="col" className="border-0">
                    MarketName
                  </th>
                  <th scope="col" className="border-0">
                    Date
                  </th>
                  <th scope="col" className="border-0">
                  UpperCount
                  </th>
                  <th scope="col" className="border-0">
                  LowerCount
                  </th>
                  <th scope="col" className="border-0">
                  ExpectedCount
                  </th>
                </tr>
              </thead>
              <tbody>
                {
                    data.map((item) => (
                        <tr key={item.Store_uid}>
                            <td>{item.Store_uid}</td>
                            <td>{item.Store_name}</td>
                            <td>{item.market_name}</td>
                            <td>{item.date}</td>
                            <td>{item.Upper_count}</td>
                            <td>{item.Lower_count}</td>
                            <td>{item.Expected_Count}</td>
                            <td/>
                        </tr>
                    ))
                    }
              </tbody>
            </table>
          
          </CardBody>
        </Card>
      </Col>
    </Row>
  </Container>
)
}; 
export default TableDataForUPL;