
import { Container, Row, Col, Card, CardHeader, CardBody } from "shards-react";

import PageTitle from "../../components/common/PageTitle";

const TableDataForTM = ({data}) =>  {
    // const 
  return(
    
<Container fluid className="main-content-container px-4">
    {/* Page Header */}
    <Row noGutters className="page-header py-4">
      <PageTitle sm="4" title="Deep Science AI" subtitle="Traffic Count" className="text-sm-left" />
    </Row>
    <Row>
      <Col>
        <Card small className="mb-4 overflow-hidden">
          <CardHeader className="bg-dark">
            <h6 className="m-0 text-white">Traffic Count </h6>
          </CardHeader>
          <CardBody className="bg-dark p-0 pb-3">
            <table className="table table-dark mb-0">
              <thead className="thead-dark">
                <tr>
                  <th scope="col" className="border-0">
                    StoreUID
                  </th>
                  <th scope="col" className="border-0">
                    Store
                  </th>
                  <th scope="col" className="border-0">
                    MarketName
                  </th>
                  <th scope="col" className="border-0">
                    Date
                  </th>
                  <th scope="col" className="border-0">
                  Str Open
                  </th>
                  <th scope="col" className="border-0">
                  Str Close
                  </th>
                  <th scope="col" className="border-0">
                  Morning
                  </th>
                  <th scope="col" className="border-0">
                  Night
                  </th>
                  <th scope="col" className="border-0">
                  Opening Time Difference
                  </th>
                  <th scope="col" className="border-0">
                  Closing Time Difference
                  </th>
                  
                </tr>
              </thead>
              <tbody>
                {
                    data.map((item) => (
                        <tr key={item.storeUID}>
                            <td>{item.storeUID}</td>
                            <td>{item.store}</td>
                            <td>{item.market}</td>
                            <td>{item.date}</td>
                            <td>{item.storeopen}</td>
                            <td>{item.storeclose}</td>
                            <td>{item.morning}</td>
                            <td>{item.night}</td>
                            <td>{item.mtimedifference}</td>
                            <td>{item.ntimedifference}</td>
                            <td/>
                        </tr>
                    ))
                    }
              </tbody>
            </table>
          
          </CardBody>
        </Card>
      </Col>
    </Row>
  </Container>
)
}; 
export default TableDataForTM;