import Offcanvas from "react-bootstrap/Offcanvas"
import Button from "react-bootstrap/Button";
import React, { useState } from "react";
import './bp.css'
// import Addingrow from "./addingrow";
function OffCanvasExample({ name, ...props }) {
    const [show, setShow] = useState(false);
  
    const handleClose = () => setShow(false);
    const handleShow = () => setShow(true);
    return (
      <>
        <Button variant="primary" onClick={handleShow} className="me-2 settingButton">
          {name="Add Store"}
        </Button>
        <Offcanvas show={show} onHide={handleClose} {...props}>
          <Offcanvas.Header closeButton>
            <Offcanvas.Title>Offcanvas</Offcanvas.Title>
          </Offcanvas.Header>
          <Offcanvas.Body>
              <p>I will use you after wait and watch</p>
              {/* <Addingrow /> */}
          </Offcanvas.Body>
        </Offcanvas>
      </>
    );
  }
  
  function SideToggle() {
    return (
      <>
      

        {['end'].map((placement, idx) => (
            <OffCanvasExample key={idx} placement={placement} name={placement} />
            ))}
      </>
    );
  }
  

export default SideToggle;