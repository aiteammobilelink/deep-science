import {Row, Col , Card, Button} from "react-bootstrap";
import {srf2} from '../components/components-overview/file.js'
import Videojs from '../components/components-overview/videoPlayer'
import './Button.css'
const ARow =(props)=>{
    const {checked, value, onChange, onChecked} = props;
    return (
      <div>
        <Row>
        <Col lg={12} md={6} sm={6} sm={12}>
        <Card className="bgrn mt-2"   value = {value} onChange={onChange} >
            <Videojs video={srf2} />
            <Card.Body className="cardBody" >
            <Card.Title className="bgrn">Perkins Rd</Card.Title>
            <Card.Text>
                <div className="mt-2">
                <Button className="btn-outline-success btp" style={{marginRight:"10px"}}>
                    Add Notes
                </Button>
                <Button className="btn-outline-success btp">
                    Save Clips
                </Button>
                </div>

            </Card.Text>
            </Card.Body>
            <Card.Footer className="cf">
            <small className="text-muted">Motion @Airline Shopping Center</small>
                <input 
                type="checkbox" 
                checked={checked}
                onChange={onChecked}
                />
            </Card.Footer>
            </Card>
            {/* <input type ="text" value={value}  onChange={onChange}/> */}
        
          </Col>    
        </Row>
      </div>
    );
  }
  export default ARow;

