
// import Addbutton from "./addbutton.js";
import React from 'react'
import ARow from "./add.js";
import {Row, Col, Button} from "react-bootstrap";
import './bp.css'
class Addingrow extends React.Component {
    constructor(props){
      super(props);
      this.state = {
        rows: [
          
        ]
      };
    }
    
    updateValue = (e, idx) => {
      const rows = [...this.state.rows];  // copy array because we don't want to mutate the previous one
      rows[idx].value = e.target.value;
      this.setState({
          rows,
      });
    }
    
    onChecked = (idx) => {
      const rows = [...this.state.rows];  // copy array because we don't want to mutate the previous one
      rows[idx].checked = !rows[idx].checked;
      this.setState({
          rows,
      });
    } 
    
    addRow = () => {
      const rows = [...this.state.rows, 
                    {value:'', checked: false}
                   ];
      this.setState({
          rows,
      });
    }
    
    deleteRows = () => {
      this.setState({
        rows: this.state.rows.filter(e => !e.checked)
      });
    }
   
    render(){
      return(
        <div>
        <span className="d-flex justify-content-start mt-2">
        
        <select name="stores" className="st" id="stores">
        <option selected>Select Store</option>
        <option value="volvo">Volvo</option>
        <option value="saab">Saab</option>
        <option value="mercedes">Mercedes</option>
        <option value="audi">Audi</option>
        </select>

        <select name="st_channels" className="st_c" id="stores">
        <option selected>Select Channel</option>
        <option value="1">1</option>
        <option value="2">2</option>
        <option value="3">3</option>
        <option value="4">4</option>
        </select>
        {/* <Addbutton add={this.addRow} */}
        {/* /> */}
        <Button onClick={this.addRow} className="addbtn"> Add Store</Button>
        <Button onClick={this.deleteRows} className="addbtn">
            delete
          </Button>
     </span>
                <Row>
        
          {this.state.rows.map((row, idx) => {
              return(
            <Col lg={4} >
               <ARow 
                key={idx} 
                value={row.value}
                checked={row.checked}
                onChecked={() => this.onChecked(idx)} 
                onChange={(e) => this.updateValue(e, idx)}
                style={{marginTop:'5px'}}
                 />
                 {console.log("row.value is containing the value of",row.value)}
                 {console.log("index is containing the value of",idx)}
            </Col>
        )
                })
            } 
              </Row>
        </div>

      );
    }
  }
export default Addingrow;