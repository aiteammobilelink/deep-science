import React from 'react'
import {Accordion} from 'react-bootstrap';
import './bp.css';

import {
    Row,
    Col,
  } from "shards-react";
import RangeDatePicker from "./../components/common/RangeDatePicker";
import RadioButtons from '../components/components-overview/RadioButtons'
// import Combo from  "../components/blog/ComboChart"
export default function AccordianComponent() {
    return (
        <div style={{}}>
            <Accordion defaultActiveKey="0">
            <Accordion.Item eventKey="0" >
              <Accordion.Header></Accordion.Header>
              <Accordion.Body style={{padding:"10px 10px 10px 10px"}}>
                <Row>
                    <Col className="col-lg-8 mb-5" >
                        {/* <Combo />    */}
                    </Col>
                        <Col className="col-lg-4">
                        <div style={{marginLeft:"30%"}}>
                        <RangeDatePicker />
                        </div>
                        <div className='mt-4 d-flex justify-content-between'>
                            <a href="#"><h5> Day </h5></a>
                            <a href="#"><h5>Week </h5></a>
                            <a href="#"><h5>Month </h5></a>
                            <a href="#"><h5>Year </h5></a>
                        </div>
                        <hr />
                            <div>
                                <RadioButtons />
                            </div>
                        </Col>
                        

                </Row>
              </Accordion.Body>
            </Accordion.Item>
            </Accordion>
        </div>
    )
}
