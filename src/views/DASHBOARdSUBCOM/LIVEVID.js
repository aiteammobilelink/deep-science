import {Row,Col,CardHeader,} from "shards-react";
import Button from 'react-bootstrap/Button'
import "../../views/Button.css"
import Card from 'react-bootstrap/Card' 
import '../../views/scrollbar.css';
import Player from "../../components/components-overview/HLS_js/Hls_Js.js"
import Player1 from "../../components/components-overview/HLS_js/Hls_Js1.js"
import Player2 from "../../components/components-overview/HLS_js/Hls_Js2.js"

export default function MediaPlayer() {

    return (
    <>
    {/* Header Spot */}
      <Card>
      <CardHeader className="border-bottom">
      <h3>Recently Viewed (3)</h3>
      </CardHeader>
      </Card>
        <div className="scrollbar scrollbar-juicy-peach">
            
            <Row className="mt-2">
                {/* First Player */}
                <Col className="col-lg-4 col-md-6 col-sm-12 col-xs-12">
                    <Card>
                    <Player/>
                    <Card.Body className="cardBody">
                    <Card.Title>Rundberg</Card.Title>
                    <Card.Text>
                    
                        <div className="">
                        <Button className="btn-outline-success btnchange1" style={{marginRight:"10px"}}>
                            Add Notes
                        </Button>
                        <Button className="btn-outline-success btnchange1">
                            Save Clips
                        </Button>
                        </div>

                    </Card.Text>
                    </Card.Body>
                    <Card.Footer style={{color:"black"}}>
                    <small className="text-muted">Motion @Airline Shopping Center</small>
                    </Card.Footer>
                    </Card>
                    </Col>
                    {/* 2nd Player */}
                    <Col className="col-lg-4 col-md-6 col-sm-12 col-xs-12">
                        <Card>
                        <Player1/>
                        <Card.Body className="cardBody">
                        <Card.Title>Tillmans Corner</Card.Title>
                        <Card.Text>
                            <div className="">
                            <Button className="btn-outline-success btnchange1" style={{marginRight:"10px"}}>
                                Add Notes
                            </Button>
                            <Button className="btn-outline-success btnchange1">
                                Save Clips
                            </Button>
                            </div>

                        </Card.Text>
                        </Card.Body>
                        <Card.Footer style={{color:"black"}}>
                        <small className="text-muted">Motion @Airline Shopping Center</small>
                        </Card.Footer>
                        </Card>
                    </Col>
                    {/* 3rd Player */}
                <Col className="col-lg-4 col-md-6 col-sm-12 col-xs-12">
                <Card>
                    <Player2/>
                    <Card.Body className="cardBody">
                    <Card.Title>Perkins Rd</Card.Title>
                    <Card.Text>
                    
                        <div className="">
                        <Button className="btn-outline-success btnchange1" style={{marginRight:"10px"}}>
                            Add Notes
                        </Button>
                        <Button className="btn-outline-success btnchange1">
                            Save Clips
                        </Button>
                        </div>

                    </Card.Text>
                    </Card.Body>
                    <Card.Footer style={{color:"black"}}>
                    <small className="text-muted">Motion @Airline Shopping Center</small>
                    </Card.Footer>
                </Card>
                </Col>
            </Row>      
        </div>
        </>
    )
}
