import { Line } from "react-chartjs-2";
const Graph = ({items,dates_int,marketdatas}) =>{
    const dates_integer=dates_int
    const selects=marketdatas
    const markets=(items,dates_integer,selects)=> {
    
        let dates=[]
        let date=items.map(item=>item.Date)
        let market=items.map(item=> item.Market)
        console.log("Date and market",date,market)
        for(let i in (date,market,items)) {
            
          if (date[i]===dates_integer && market[i]===selects) {
            dates.push(items[i])
          }
        }
        return dates;
      }
     
      const data=markets(items,dates_integer,selects)
      const IncomingTraffic=data.map((trafic)=>trafic.IncomingTraffic)
      const location=data.map((loca)=>loca.Location)
      const Sales=data.map((sal)=>sal.Sale)
    return(
        <div>
        <Line
            data={{
                labels:location,
                datasets:[
                    {
                        label:"Traffic Count",
                        data:IncomingTraffic,
                        backgroundColor:[
                            'rgba(255, 99, 132, 0.2)',
                            'rgba(255, 159, 64, 0.2)',
                            'rgba(255, 205, 86, 0.2)',
                            'rgba(75, 192, 192, 0.2)',
                            'rgba(54, 162, 235, 0.2)',
                            'rgba(153, 102, 255, 0.2)',
                            'rgba(201, 203, 207, 0.2)'
                        ],
                        borderColor:[
                            'rgb(255, 99, 132)',
                            'rgb(255, 159, 64)',
                            'rgb(255, 205, 86)',
                            'rgb(75, 192, 192)',
                            'rgb(54, 162, 235)',
                            'rgb(153, 102, 255)',
                            'rgb(201, 203, 207)'
                        ],
                        borderWidth:4,
                    },
                    {
                    label:"Sales",
                    data:Sales,
                    backgroundColor:[
                        'rgba(255, 99, 132, 0.2)',
                        'rgba(255, 159, 64, 0.2)',
                        'rgba(255, 205, 86, 0.2)',
                        'rgba(75, 192, 192, 0.2)',
                        'rgba(54, 162, 235, 0.2)',
                        'rgba(153, 102, 255, 0.2)',
                        'rgba(201, 203, 207, 0.2)'
                    ],
                    borderColor:[
                        // 'rgb(255, 99, 132)',
                        // 'rgb(255, 159, 64)',
                        // 'rgb(255, 205, 86)',
                        'rgb(75, 192, 192)',
                        'rgb(54, 162, 235)',
                        'rgb(153, 102, 255)',
                        'rgb(201, 203, 207)'
                    ],
                    borderWidth:4,
                }
                ],
            }
        }

            width="100%"
            height="30%"
           
            />
          </div>

    )
}
export default Graph;