import React from "react";
import { Redirect } from "react-router-dom";

// Layout Types
import { DefaultLayout } from "./layouts";

// Route Views
import DashOverview from "./views/mainSiteComponents/DASHBOARD/Dashboard";
import Predictive from "./views/mainSiteComponents/PREDICTIVE/Predictive";
import VideosComponent from "./views/mainSiteComponents/SURVEILLANCEMAIN/surveillancelive";
import Errors from "./views/Errors";
import Clips from "./views/mainSiteComponents/VIDEOCLIPS/clips.js";
import Tables from "./views/mainSiteComponents/TABLES/Tables";
import Events from "./views/mainSiteComponents/EVENTS/eventSurveillance";
import AI from "./views/mainSiteComponents/AITRACKER/AItracker";
import Fvideos from "./views/mainSiteComponents/FOURVIEWSURVEILLANCE/FourView.js";
import SFvideos from "./views/mainSiteComponents/FOURVIEWSURVEILLANCE/SurveillanceFview";
import SEvideos from './views/mainSiteComponents/SEVIEWS/SEViews'
import Timemonitor from "./views/TimeMonitor/timemonitor.js"; 

export default [
  {
    path: "/",
    exact: true,
    layout: DefaultLayout,
    component: () => <Redirect to="/dashboard" />
  },
  {
    path: "/dashboard",
    layout: DefaultLayout,
    component: DashOverview
  },
  {
    path: "/predictive",
    layout: DefaultLayout,
    component: Predictive
  },
  {
    path: "/video",
    layout: DefaultLayout,
    component: VideosComponent
  },
  {
    path: "/ai",
    layout: DefaultLayout,
    component: AI
  },
  {
    path: "/errors",
    layout: DefaultLayout,
    component: Errors
  },
  {
    path: "/clips",
    layout: DefaultLayout,
    component: Clips
  },
  {
    path: "/tables",
    layout: DefaultLayout,
    component: Tables
  },
  {
    path: "/event",
    layout: DefaultLayout,
    component: Events
  },
  {
    path:"/fview",
    layout:DefaultLayout,
    component:Fvideos
  },
  {
    path:"/SurveillanceView",
    layout:DefaultLayout,
    component:SFvideos
  },
  {
    path:"/SEView",
    layout:DefaultLayout,
    component:SEvideos
  },
  {
    path:"/timemonitoring",
    layout:DefaultLayout,
    component:Timemonitor
  }
];
